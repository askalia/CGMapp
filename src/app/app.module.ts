import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import {RouterModule} from "@angular/router";
import { AppRoutes } from './app.routing';
import { LandingpageLayoutComponent } from './layouts/landingpage/landingpage-layout.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { LandingBodyComponent } from './layouts/landing-body/landing-body.component';
import { LandingHeaderComponent } from './layouts/landing-header/landing-header.component';
import { DefaultComponent } from './pages/default/default.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AuthService} from "./services/auth.service";
import { ListitemsComponent } from './pages/listitems/listitems.component';
import {PersonsService} from "./services/persons.service";
import { OrderByPipe } from './pipes/order-by.pipe';
import { DetailItemComponent } from './pages/detail-item/detail-item.component';
import { PersonAlldetailsComponent } from './webcomponents/person-alldetails/person-alldetails.component';
import { PersonHeaderAlldetailsComponent } from './webcomponents/person-header-alldetails/person-header-alldetails.component';
import { PersonAlldetailsExpandableComponent } from './webcomponents/person-alldetails-expandable/person-alldetails-expandable.component';

@NgModule({
    declarations: [
        AppComponent,
        LandingpageLayoutComponent,
        LoginComponent,
        LandingBodyComponent,
        LandingHeaderComponent,
        DefaultComponent,
        RegisterComponent,
        ListitemsComponent,
        OrderByPipe,
        DetailItemComponent,
        PersonAlldetailsComponent,
        PersonHeaderAlldetailsComponent,
        PersonAlldetailsExpandableComponent
    ],
    imports: [
        BrowserModule,
        NgbModule.forRoot(),
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot(AppRoutes)
    ],
    providers: [ AuthService, PersonsService],
    bootstrap: [AppComponent]
})
export class AppModule { }
