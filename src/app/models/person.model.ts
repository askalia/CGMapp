
  export class Person {
    id: number;
    name: string;
    username: string;
    email: string;
    address: any;
    phone: string;
    _website: string;
    company: any;

    get website(): string{
      return (this._website.indexOf('http') > -1 ? '' : 'https://') +
             this._website;
    }

    set website(value){
      this._website = value;
    }

    get fullname(): string{
      return [ this.firstname, this.lastname.toUpperCase()].join(' ');
    }

    get lastname(): string{
      // for data isolation principe, I prefering working on a copy of the value to be transformed.
      // even though the split() method returns a brand new array, and keeps the object's name property safe
      return (''+this.name).split(' ').pop();
    }

    get firstname(): string{
      // Provided 'lastname' and 'firstname' are always provided with the position pattern (this API don't move the way)
      // I get rid of the lastname segment, then return the rest
      const splitter = (''+this.name).split(' ');
      splitter.pop();
      return splitter.join(' ');
    }

}

