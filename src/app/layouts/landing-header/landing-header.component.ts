import { Component, OnInit } from '@angular/core';
import { Router} from "@angular/router";

@Component({
  selector: 'app-landing-header',
  templateUrl: './landing-header.component.html',
  styleUrls: ['./landing-header.component.css']
})
export class LandingHeaderComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  displayLoginButton(): boolean{
    return (this.router.url !== '/auth/login');
  }

  displayRegisterButton(): boolean{
    return (this.router.url !== '/auth/register');
  }

}
