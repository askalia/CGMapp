import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingpageLayoutComponent } from './landingpage-layout.component';

describe('LandingpageLayoutComponent', () => {
  let component: LandingpageLayoutComponent;
  let fixture: ComponentFixture<LandingpageLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingpageLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingpageLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
