import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonAlldetailsComponent } from './person-alldetails.component';

describe('PersonAlldetailsComponent', () => {
  let component: PersonAlldetailsComponent;
  let fixture: ComponentFixture<PersonAlldetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonAlldetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonAlldetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
