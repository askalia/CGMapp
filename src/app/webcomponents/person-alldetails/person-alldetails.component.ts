import {Component, OnInit, Input} from '@angular/core';
import {Person} from "../../models/person.model";

@Component({
  selector: 'app-person-alldetails',
  templateUrl: './person-alldetails.component.html',
  styleUrls: ['./person-alldetails.component.css']
})
export class PersonAlldetailsComponent implements OnInit {

  @Input() person: Person;

  constructor() { }

  ngOnInit() {
  }

}
