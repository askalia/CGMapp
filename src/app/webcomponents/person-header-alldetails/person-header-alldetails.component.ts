import {Component, OnInit, Input} from '@angular/core';
import {Person} from "../../models/person.model";

@Component({
  selector: 'app-person-header-alldetails',
  templateUrl: './person-header-alldetails.component.html',
  styleUrls: ['./person-header-alldetails.component.css']
})
export class PersonHeaderAlldetailsComponent implements OnInit {

  @Input() person: Person;

  constructor() { }

  ngOnInit() {
  }

}
