import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonHeaderAlldetailsComponent } from './person-header-alldetails.component';

describe('PersonHeaderAlldetailsComponent', () => {
  let component: PersonHeaderAlldetailsComponent;
  let fixture: ComponentFixture<PersonHeaderAlldetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonHeaderAlldetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonHeaderAlldetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
