import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonAlldetailsExpandableComponent } from './person-alldetails-expandable.component';

describe('PersonAlldetailsExpandableComponent', () => {
  let component: PersonAlldetailsExpandableComponent;
  let fixture: ComponentFixture<PersonAlldetailsExpandableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonAlldetailsExpandableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonAlldetailsExpandableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
