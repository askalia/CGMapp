import {Component, OnInit, Input} from '@angular/core';
import {Person} from "../../models/person.model";

@Component({
  selector: 'app-person-alldetails-expandable',
  templateUrl: './person-alldetails-expandable.component.html',
  styleUrls: ['./person-alldetails-expandable.component.css']
})
export class PersonAlldetailsExpandableComponent implements OnInit {

  @Input() person: Person;

  constructor() { }

  ngOnInit() {
  }

}
