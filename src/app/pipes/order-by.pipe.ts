import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  transform(array, orderBy, asc = true){



    if (!orderBy || orderBy.trim() == ""){
      return array;
    }

    if (array === null || typeof array === 'undefined'){
      return array;
    }

    //ascending
    if (asc){
      array.sort(function() {
        console.log('coin');
      });

      return Array.from(array).sort((item1: any, item2: any) => {
        return this.orderByComparator(item1[orderBy], item2[orderBy]);
      });
    }
    else{
      //not asc
      return Array.from(array).sort((item1: any, item2: any) => {
        return this.orderByComparator(item2[orderBy], item1[orderBy]);
      });
    }

  }

  orderByComparator(currentElement:any, nextElement:any):number{

    if((isNaN(parseFloat(currentElement)) || !isFinite(currentElement)) || (isNaN(parseFloat(nextElement)) || !isFinite(nextElement))){
      //Isn't currentElement number so lowercase the string to properly compare
      if(currentElement.toLowerCase() < nextElement.toLowerCase()) return -1;
      if(currentElement.toLowerCase() > nextElement.toLowerCase()) return 1;
    }
    else{
      //Parse strings as numbers to compare properly
      if(parseFloat(currentElement) < parseFloat(nextElement)) return -1;
      if(parseFloat(currentElement) > parseFloat(nextElement)) return 1;
    }

    return 0; //equal each other
  }

}
