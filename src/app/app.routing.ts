import { Routes } from '@angular/router';
import {LandingpageLayoutComponent} from "./layouts/landingpage/landingpage-layout.component";
import {LoginComponent} from "./pages/auth/login/login.component";
import {LandingBodyComponent} from "./layouts/landing-body/landing-body.component";
import {ListitemsComponent} from "./pages/listitems/listitems.component";
import {DetailItemComponent} from "./pages/detail-item/detail-item.component";
import {RegisterComponent} from "./pages/auth/register/register.component";

export const AppRoutes: Routes = [
  {
    path : '',
    component: LandingpageLayoutComponent,
    children : [
      {
        path : '',
        component : LandingBodyComponent
      },
      {
        path: 'persons',
        children : [
          {
            path : '',
            component : ListitemsComponent
          },
          {
            path : ':id',
            component: DetailItemComponent
          }
        ]
      }

    ],
  },
  {
    path : 'auth',
    component: LandingpageLayoutComponent,
    children : [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      }
    ]
  }
];
