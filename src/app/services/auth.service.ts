import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class AuthService {

  constructor() { }

  authenticate(credentials): Observable<boolean>{
    return Observable.create((observer) => {
      observer.next( true );
      observer.complete();
    });
  }

}
