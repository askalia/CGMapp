import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Person} from "../models/person.model";
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';


@Injectable()
export class PersonsService {

  constructor(private http: Http) { }

  list(): Observable<Array<Person>> {
    return this.http.get(`${environment.personsApiUrl}/users`)
      .map(res => res.json());
  }

  detail(id): Observable<Person> {
      return this.http.get(`${environment.personsApiUrl}/users/${id}`)
        .map(res => Object.assign(new Person(), res.json()));
  }
}
