import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, FormBuilder, Validators, ValidationErrors} from "@angular/forms";
import * as moment from 'moment';
import {environment} from "../../../../environments/environment";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  firstnameCtrl   : FormControl;
  familynameCtrl  : FormControl;
  dateOfBirthCtrl : FormControl;
  passwordCtrl    : FormControl;
  emailCtrl     : FormControl;

  /**
   * This function check if the user attempting to register has min age required
   * @param control
   * @returns null | {{tooYoung: boolean}}
   */
  static checkUserHasMajority(control: FormControl): ValidationErrors | null{

    const inputTime = moment([control.value.year, control.value.month, control.value.day+1].join('-'));

    if (! inputTime.isValid()){
      return {
        invalidFormat: true
      }
    }
    const diffTime = moment().diff(inputTime);
    const diffAsYears  = Math.floor(moment.duration(diffTime).asYears());
    return (diffAsYears >= environment.minAge) ? null : { tooYoung: true };
  }

  constructor(private fBuilder: FormBuilder, private router: Router) {

    // here I set validation rules on each form field.
    // we can consider namin these fields as "controls" once they are set up.
    this.firstnameCtrl    = this.fBuilder.control('', [ Validators.required, Validators.minLength(3) ]);
    this.familynameCtrl   = this.fBuilder.control('', [ Validators.required, Validators.minLength(3) ]);
    this.dateOfBirthCtrl  = this.fBuilder.control('', [ Validators.required, RegisterComponent.checkUserHasMajority ]);
    this.passwordCtrl     = this.fBuilder.control('', [ Validators.required, Validators.minLength(3) ]);
    this.emailCtrl        = this.fBuilder.control('', [ Validators.required, Validators.email ]);

    // this form binded with the HTML form is made up of those form controls
    this.registerForm = this.fBuilder.group({
      firstname   : this.firstnameCtrl,
      familyname  : this.familynameCtrl,
      dateOfBirth : this.dateOfBirthCtrl,
      password    : this.passwordCtrl,
      email       : this.emailCtrl
    })
  }

  ngOnInit() {
  }

  register(){
    // I got not ui-bootstrap datepicker to work plenty. However the binded form-control remains invalid because no date can ben picked.
    // @todo : this hack has to be fixed
    if (this.registerForm.valid || (!this.registerForm.valid && !this.registerForm.get('dateOfBirth').valid)) {
      this.router.navigate(['auth/login']);
    }
  }

}
