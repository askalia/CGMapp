import {Component, OnInit, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import {AuthService} from "../../../services/auth.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

    authEventsSubscriber: Subscription;
    authenticationFailed: boolean = false;
    credentials: any = {
        email: '',
        password: ''
      };

    constructor(
        private authService: AuthService,
        private router: Router
    ) { }


    authenticate() {
        this.authenticationFailed = false;
        this.authEventsSubscriber = this.authService.authenticate(this.credentials)
          .subscribe(
            () => this.router.navigate(['persons']),
            () => this.authenticationFailed = true
          );

  }

  ngOnInit() {
  }

  ngOnDestroy(){
      if (this.authEventsSubscriber){
        this.authEventsSubscriber.unsubscribe();
      }
  }

}
