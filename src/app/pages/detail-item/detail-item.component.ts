import {Component, OnInit, OnDestroy} from '@angular/core';
import {PersonsService} from "../../services/persons.service";
import {Person} from "../../models/person.model";
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-detail-item',
  templateUrl: './detail-item.component.html',
  styleUrls: ['./detail-item.component.css']
})
export class DetailItemComponent implements OnInit, OnDestroy {

  person: Person;
  personsEventsSubcriber: Subscription;

  constructor(
    private personsService: PersonsService,
    private route: ActivatedRoute)
  { }

  ngOnInit() {
      this.personsEventsSubcriber = this.personsService.detail(this.route.snapshot.paramMap.get('id'))
        .subscribe((person: Person) => {
          this.person = person;
        });
  }

  ngOnDestroy(){
    if (this.personsEventsSubcriber){
      this.personsEventsSubcriber.unsubscribe();
    }
  }

}
