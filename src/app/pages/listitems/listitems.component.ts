import { Component, OnInit } from '@angular/core';
import {Person} from "../../models/person.model";
import {PersonsService} from "../../services/persons.service";

@Component({
  selector: 'app-listitems',
  templateUrl: './listitems.component.html',
  styleUrls: ['./listitems.component.css']
})
export class ListitemsComponent implements OnInit {

  persons : Array<Person> = new Array<Person>();
  orderProp: String = 'lastname';
  orderAsc: boolean = false;

  constructor(
    private personsService: PersonsService
  ) { }

  ngOnInit() {

    this.personsService.list()
      .subscribe((persons: Array<Person>) => {
        this.persons  = persons.map(el => Object.assign(new Person(), el));
        //persons.forEach(el => {
//          this.persons.push(Object.assign(new Person(), el));
  //      });
      })



  }

}
