export const environment = {
  production: true,
  personsApiUrl: 'https://jsonplaceholder.typicode.com',
  minAge: 18
};
