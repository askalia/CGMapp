import { CGMappPage } from './app.po';

describe('cgmapp App', () => {
  let page: CGMappPage;

  beforeEach(() => {
    page = new CGMappPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
